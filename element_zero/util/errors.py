class EmojiException(Exception):
    pass


class DiscordInterfaceError(EmojiException):
    pass


class UserError(EmojiException):
    """Exception class for errors caused as a direct result of user behavior."""
    pass


class BackendGuildsFull(UserError):
    def __init__(self):
        super().__init__('All backend servers are full.')


class UserAtCapacity(UserError):
    def __init__(self, slot_limit):
        super().__init__(f'User has reached their slot limit of {slot_limit}.')


class UserBlacklisted(UserError):
    def __init__(self, case):
        super().__init__(f'User has been blacklisted with the reason `{case.get("reason")}`.')


class UserVerificationFailed(UserError):
    def __init__(self):
        super().__init__(
            'User does not have access to modify the emoji in question. '
            'If you wish to report the emoji for being inappropriate, please use the report command.'
        )


class EmojiExists(UserError):
    def __init__(self):
        super().__init__('An emoji with this name already exists.')


class EmojiNotFound(UserError):
    def __init__(self):
        super().__init__('An emoji with this name does not exist.')


class InvalidCharacterInName(UserError):
    def __init__(self, char):
        char = char.replace('`', '˴')
        super().__init__(f'The character `{char}` is not allowed in emoji names.')
